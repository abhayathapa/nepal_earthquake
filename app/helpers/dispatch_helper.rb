module DispatchHelper
  def dispatch_status
    [
      "Ready to Dispatch",
      "Dispatched",
      "Delivered"
    ]
  end
end