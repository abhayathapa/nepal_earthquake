class DispatchedItemsController < ApplicationController
  before_action :set_dispatched_item, only: [:show, :edit, :update, :destroy]
  before_action :set_dispatch
  respond_to :html

  def index
    @dispatched_items = DispatchedItem.all
    respond_with(@dispatch, @dispatched_item)
  end

  def show
    respond_with(@dispatch, @dispatched_item)
  end

  def new
    @dispatched_item = DispatchedItem.new
    respond_with(@dispatch, @dispatched_item)
  end

  def edit
  end

  def create
    @dispatched_item = DispatchedItem.new(dispatched_item_params)
    @dispatched_item.dispatch_id = params['dispatch_id']
    @dispatched_item.save
    redirect_to dispatch_dispatched_items_path(@dispatch)
  end

  def update
    stock_adjust = @dispatched_item.amount - dispatched_item_params['amount'].to_i
    @dispatched_item.update(dispatched_item_params)
    inventory = @dispatched_item.inventory
    inventory.update_attributes(amount: inventory.amount + stock_adjust)
    redirect_to dispatch_dispatched_items_path(@dispatch)
  end

  def destroy
    @dispatched_item.destroy
    redirect_to dispatch_dispatched_items_path(@dispatch)
  end

  private
    def set_dispatched_item
      @dispatched_item = DispatchedItem.find(params[:id])
    end

    def set_dispatch
      @dispatch = Dispatch.find params['dispatch_id']
    end

    def dispatched_item_params
      params.require(:dispatched_item).permit(:dispatch_id, :inventory_id, :amount, :unit)
    end
end
