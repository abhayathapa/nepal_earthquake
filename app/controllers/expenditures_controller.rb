class ExpendituresController < ApplicationController
  before_action :set_expenditure, only: [:show, :edit, :update, :destroy]

  respond_to :html

  def index
    @expenditures = Expenditure.all
    respond_with(@expenditures)
  end

  def show
    respond_with(@expenditure)
  end

  def new
    @expenditure = Expenditure.new
    respond_with(@expenditure)
  end

  def edit
  end

  def create
    @expenditure = Expenditure.new(expenditure_params)
    @expenditure.save
    redirect_to expenditures_path
  end

  def update
    @expenditure.update(expenditure_params)
    respond_with(@expenditure)
  end

  def destroy
    @expenditure.destroy
    respond_with(@expenditure)
  end

  private
    def set_expenditure
      @expenditure = Expenditure.find(params[:id])
    end

    def expenditure_params
      params.require(:expenditure).permit(:amount, :units, :name, :date)
    end
end
