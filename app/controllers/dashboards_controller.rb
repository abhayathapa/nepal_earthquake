class DashboardsController < ApplicationController

  respond_to :html

  def index
    @expenses = "Rs " + (Procurement.expenses + DispatchedItem.expenses + Expenditure.expenses).to_s
    @income = "Rs " + Item.income.to_s
    @dispatches_today = Dispatch.today
    @dispatches_alltime = Dispatch.archive
    @inventories = Inventory.all
  end
end
