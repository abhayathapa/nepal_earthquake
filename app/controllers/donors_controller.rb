class DonorsController < ApplicationController
  before_action :set_donor, only: [:show, :edit, :update, :destroy]

  respond_to :html

  def index
    @donors = Donor.all
    respond_with(@donors)
  end

  def show
    respond_with(@donor)
  end

  def new
    @donor = Donor.new
    respond_with(@donor)
  end

  def edit
  end

  def create
    @donor = Donor.new(donor_params)
    @donor.save
    redirect_to donors_path
  end

  def update
    @donor.update(donor_params)
    respond_with(@donor)
  end

  def destroy
    @donor.destroy
    respond_with(@donor)
  end

  private
    def set_donor
      @donor = Donor.find(params[:id])
    end

    def donor_params
      params.require(:donor).permit(:name, :address, :phone_number, :email)
    end
end
