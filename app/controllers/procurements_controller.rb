class ProcurementsController < ApplicationController
  before_action :set_procurement, only: [:show, :edit, :update, :destroy]

  respond_to :html

  def index
    @procurements = Procurement.all
    respond_with(@procurements)
  end

  def show
    respond_with(@procurement)
  end

  def new
    @procurement = Procurement.new
    respond_with(@procurement)
  end

  def edit
  end

  def create
    @procurement = Procurement.new(procurement_params)
    @procurement.save
    redirect_to procurements_path
  end

  def update
    @procurement.update(procurement_params)
    respond_with(@procurement)
  end

  def destroy
    @procurement.destroy
    respond_with(@procurement)
  end

  private
    def set_procurement
      @procurement = Procurement.find(params[:id])
    end

    def procurement_params
      params.require(:procurement).permit(:amount, :units, :date, :inventory_id, :funds_used)
    end
end
