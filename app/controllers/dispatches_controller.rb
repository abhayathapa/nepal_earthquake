class DispatchesController < ApplicationController
  before_action :set_dispatch, only: [:show, :edit, :update, :destroy]

  respond_to :html

  def index
    @dispatches = Dispatch.all
    respond_with(@dispatches)
  end

  def show
    respond_with(@dispatch)
  end

  def new
    @dispatch = Dispatch.new
    respond_with(@dispatch)
  end

  def edit
  end

  def create
    @dispatch = Dispatch.new(dispatch_params)
    @dispatch.save
    redirect_to dispatch_dispatched_items_path(@dispatch)
  end

  def update
    @dispatch.update(dispatch_params)
    respond_with(@dispatch)
  end

  def destroy
    @dispatch.destroy
    respond_with(@dispatch)
  end

  private
    def set_dispatch
      @dispatch = Dispatch.find(params[:id])
    end

    def dispatch_params
      params.require(:dispatch).permit(:name, :description, :site_id, :dispatch_date, :status, :feedback)
    end
end
