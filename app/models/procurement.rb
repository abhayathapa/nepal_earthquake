class Procurement < ActiveRecord::Base
  belongs_to :inventory
  after_create :add_to_inventory, :deduct_funds

  validates_presence_of :amount, :inventory, :funds_used

  def self.expenses
    all.collect(&:funds_used).sum
  end

  private
    def deduct_funds
      cash_inventory = Inventory.find_by_inventory_type('fund')
      cash_inventory.amount -= funds_used
      cash_inventory.save
    end

    def add_to_inventory
      self.inventory.amount += amount
      self.inventory.save
    end
end
