class Dispatch < ActiveRecord::Base
  belongs_to :site
  has_many :dispatched_items

  validates_presence_of :name, :dispatch_date, :site

  class << self
    def today
      all.where(dispatch_date: Date.today)
    end

    def archive
      all.order(dispatch_date: :desc)
    end
  end

  def dispatched_at
    dispatch_date
  end
end
