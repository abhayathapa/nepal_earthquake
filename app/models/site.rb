class Site < ActiveRecord::Base
  has_many :dispatches
  has_many :requests
end
