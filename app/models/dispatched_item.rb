class DispatchedItem < ActiveRecord::Base
  belongs_to :dispatch
  belongs_to :inventory

  after_create :deduct_from_inventory

  validates_presence_of :amount, :inventory, :dispatch

  def self.expenses
    cash_inventory = Inventory.find_by_inventory_type('fund')
    DispatchedItem.where(inventory: cash_inventory).collect(&:amount).sum
  end

  private
    def deduct_from_inventory
      self.inventory.amount -= amount
      self.inventory.save
    end
end
