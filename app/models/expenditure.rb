class Expenditure < ActiveRecord::Base
  after_create :deduct_funds

  validates_presence_of :amount, :name

  def self.expenses
    all.collect(&:amount).sum
  end

  private
  def deduct_funds
    cash_inventory = Inventory.find_by_inventory_type('fund')
    cash_inventory.amount -= amount
    cash_inventory.save
  end
end
