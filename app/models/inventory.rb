class Inventory < ActiveRecord::Base
  validates :name, uniqueness: { scope: :inventory_type }
  def self.types
    all.select(:id, :name)
  end
end
