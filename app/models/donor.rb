class Donor < ActiveRecord::Base
  has_many :items

  validates_presence_of :name

  def self.list
    all.select(:id, :name)
  end
end
