class Item < ActiveRecord::Base
  belongs_to :inventory
  belongs_to :donor

  after_create :add_to_inventory

  validates_presence_of :amount, :inventory

  def self.income
    cash_inventory = Inventory.find_by_inventory_type('fund')
    Item.where(inventory: cash_inventory).collect(&:amount).sum
  end

  private
    def add_to_inventory
      self.inventory.amount += amount
      self.inventory.save
    end
end
