$ ->
  setInventoryItems= ->
    parent_type = $('#inventory_parent_type').find(':selected').val()
    options = $('#all_inventory_types option[data-type=' + parent_type + ']')
    $('.inventory-type-select select').html(options.clone())

  setUnit= ->
    unit = $('.inventory-type-select select').find(':selected').attr('data-unit')
    $('.unit-input').val(unit)

  # on page load, set inventory items based on current inventory type
  setInventoryItems()

  # on page load, set unit based on current inventory type/item
  setUnit()

  # set inventory items when inventory type is selected
  $('#inventory_parent_type').on 'change', ->
    setInventoryItems()
    setUnit()

  # set unit when another inventory type is selected
  $('.inventory-type-select select').on 'change', ->
    setUnit()

