json.array!(@inventories) do |inventory|
  json.extract! inventory, :id, :inventory_type, :name, :amount, :unit
  json.url inventory_url(inventory, format: :json)
end
