json.array!(@donors) do |donor|
  json.extract! donor, :id, :name, :address, :phone_number, :email
  json.url donor_url(donor, format: :json)
end
