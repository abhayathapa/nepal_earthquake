json.array!(@expenditures) do |expenditure|
  json.extract! expenditure, :id, :amount, :units, :name, :date
  json.url expenditure_url(expenditure, format: :json)
end
