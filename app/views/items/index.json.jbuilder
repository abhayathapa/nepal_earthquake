json.array!(@items) do |item|
  json.extract! item, :id, :name, :amount, :units, :submit_date, :donor_id, :inventory_id
  json.url item_url(item, format: :json)
end
