json.array!(@dispatched_items) do |dispatched_item|
  json.extract! dispatched_item, :id, :dispatch_id, :inventory_id, :amount, :unit
  json.url dispatched_item_url(dispatched_item, format: :json)
end
