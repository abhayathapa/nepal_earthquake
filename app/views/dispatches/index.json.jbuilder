json.array!(@dispatches) do |dispatch|
  json.extract! dispatch, :id, :name, :description, :site_id, :dispatch_date, :status, :feedback
  json.url dispatch_url(dispatch, format: :json)
end
