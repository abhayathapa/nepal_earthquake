json.array!(@volunteers) do |volunteer|
  json.extract! volunteer, :id, :name, :phone, :address, :date, :email, :department
  json.url volunteer_url(volunteer, format: :json)
end
