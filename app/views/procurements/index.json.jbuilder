json.array!(@procurements) do |procurement|
  json.extract! procurement, :id, :amount, :units, :date, :inventory_id, :funds_used
  json.url procurement_url(procurement, format: :json)
end
