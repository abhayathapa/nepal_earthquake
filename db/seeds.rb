# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

User.create!(email: "admin@jainepal.com", password: 'jainepal')

Donor.create(name:"Jai Nepal Youth Group", address: "Kathmandu")

inventory_items = [
  {
    inventory_type: "food",
    name: "noodles",
    amount: "0",
    unit: "pkgs"
  },
  {
    inventory_type: "food",
    name: "rice",
    amount: "0",
    unit: "kg"
  },
  {
    inventory_type: "food",
    name: "lentil",
    amount: "0",
    unit: "kg"
  },
  {
    inventory_type: "medicine",
    name: "cetamol",
    amount: "0",
    unit: "pkgs"
  },
  {
    inventory_type: "fund",
    name: "cash",
    amount: "0",
    unit: "Rs"
  }
]

inventory_items.each do |item|
  Inventory.create! item
end

site = Site.create(name: 'Sipa Pokhari', district:'Sindhupalchowk', vdc: '2')
dispatch = Dispatch.create(description: 'Dispatched now', dispatch_date: Date.today, site: site, name: 'name')
dispatch = Dispatch.create(description: 'Dispatched now', dispatch_date: Date.yesterday, site: site, name: 'name')
