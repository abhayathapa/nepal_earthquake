# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150502073208) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"
  enable_extension "hstore"

  create_table "dispatched_items", force: true do |t|
    t.integer  "dispatch_id"
    t.integer  "inventory_id"
    t.integer  "amount"
    t.boolean  "returned",     default: false
    t.string   "unit"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "dispatched_items", ["dispatch_id"], name: "index_dispatched_items_on_dispatch_id", using: :btree
  add_index "dispatched_items", ["inventory_id"], name: "index_dispatched_items_on_inventory_id", using: :btree

  create_table "dispatches", force: true do |t|
    t.string   "name"
    t.string   "description"
    t.integer  "site_id"
    t.date     "dispatch_date"
    t.string   "status"
    t.string   "feedback"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "dispatches", ["site_id"], name: "index_dispatches_on_site_id", using: :btree

  create_table "donors", force: true do |t|
    t.string   "name"
    t.string   "address"
    t.string   "phone_number"
    t.string   "email"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "expenditures", force: true do |t|
    t.integer  "amount"
    t.string   "category"
    t.date     "date"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "inventories", force: true do |t|
    t.string   "inventory_type"
    t.string   "name"
    t.integer  "amount"
    t.string   "unit"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "items", force: true do |t|
    t.string   "name"
    t.integer  "amount"
    t.string   "units"
    t.date     "submit_date"
    t.integer  "donor_id"
    t.integer  "inventory_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "items", ["donor_id"], name: "index_items_on_donor_id", using: :btree
  add_index "items", ["inventory_id"], name: "index_items_on_inventory_id", using: :btree

  create_table "medicine_supplies", force: true do |t|
    t.string   "name"
    t.integer  "amount"
    t.string   "units"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "requests", force: true do |t|
    t.string   "name"
    t.text     "description"
    t.string   "contact_number"
    t.string   "address"
    t.integer  "site_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "sites", force: true do |t|
    t.string   "name"
    t.string   "district"
    t.string   "vdc"
    t.string   "location"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "stocks", force: true do |t|
    t.hstore   "food"
    t.hstore   "medicine"
    t.hstore   "tents"
    t.hstore   "miscellaneous"
    t.string   "cash"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "users", force: true do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

  create_table "volunteers", force: true do |t|
    t.string   "name"
    t.integer  "phone"
    t.string   "address"
    t.date     "date"
    t.string   "email"
    t.string   "department"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

end
