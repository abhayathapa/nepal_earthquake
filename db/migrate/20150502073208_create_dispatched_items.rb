class CreateDispatchedItems < ActiveRecord::Migration
  def change
    create_table :dispatched_items do |t|
      t.references :dispatch, index: true
      t.references :inventory, index: true
      t.integer :amount
      t.boolean :returned, default: false
      t.string :unit

      t.timestamps
    end
  end
end
