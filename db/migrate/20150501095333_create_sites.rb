class CreateSites < ActiveRecord::Migration
  def change
    create_table :sites do |t|
      t.string :name
      t.string :district
      t.string :vdc
      t.string :location

      t.timestamps
    end
  end
end
