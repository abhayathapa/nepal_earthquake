class ChangePhoneNumberFromIntegerToFloat < ActiveRecord::Migration
  def change
    change_column :volunteers, :phone, :string
  end
end
