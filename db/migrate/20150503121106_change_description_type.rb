class ChangeDescriptionType < ActiveRecord::Migration
  def change
    change_column :dispatches, :description, :text
    change_column :dispatches, :feedback, :text
  end
end
