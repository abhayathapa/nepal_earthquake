class CreateProcurements < ActiveRecord::Migration
  def change
    create_table :procurements do |t|
      t.integer :amount
      t.string :units
      t.date :date
      t.references :inventory, index: true
      t.integer :funds_used

      t.timestamps
    end
  end
end
