class CreateExpenditures < ActiveRecord::Migration
  def change
    create_table :expenditures do |t|
      t.integer :amount
      t.string :units
      t.string :name
      t.date :date

      t.timestamps
    end
  end
end
