class CreateDispatches < ActiveRecord::Migration
  def change
    create_table :dispatches do |t|
      t.string :name
      t.text :description
      t.references :site, index: true
      t.date :dispatch_date
      t.string :status
      t.text :feedback

      t.timestamps
    end
  end
end
