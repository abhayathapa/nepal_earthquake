class CreateRequests < ActiveRecord::Migration
  def change
    create_table :requests do |t|
      t.string :name
      t.text :description
      t.string :contact_number
      t.string :address
      t.integer :site_id
      t.timestamps
    end
  end
end
