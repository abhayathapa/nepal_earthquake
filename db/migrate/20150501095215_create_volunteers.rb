class CreateVolunteers < ActiveRecord::Migration
  def change
    create_table :volunteers do |t|
      t.string :name
      t.integer :phone
      t.string :address
      t.date :date
      t.string :email
      t.string :department

      t.timestamps
    end
  end
end
