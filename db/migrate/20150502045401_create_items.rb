class CreateItems < ActiveRecord::Migration
  def change
    create_table :items do |t|
      t.string :name
      t.integer :amount
      t.string :units
      t.date :submit_date
      t.references :donor, index: true
      t.references :inventory, index: true

      t.timestamps
    end
  end
end
