class CreateInventories < ActiveRecord::Migration
  def change
    create_table :inventories do |t|
      t.string :inventory_type
      t.string :name
      t.integer :amount
      t.string :unit

      t.timestamps
    end
  end
end
